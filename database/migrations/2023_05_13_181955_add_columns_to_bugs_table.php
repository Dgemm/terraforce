<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bugs', function (Blueprint $table) {
            $table->integer('status_id')->nullable();
            $table->integer('mark_id')->nullable();
            $table->string('severity')->nullable();
            $table->string('browser')->nullable();
            $table->string('os')->nullable();
            $table->string('link')->nullable();
            $table->string('os_version')->nullable();
            $table->string('other_link')->nullable();
            $table->string('priority')->nullable();
            $table->text('reproduse')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bugs', function (Blueprint $table) {
            $table->dropColumn('status_id');
            $table->dropColumn('mark_id');
            $table->dropColumn('severity');
            $table->dropColumn('browser');
            $table->dropColumn('os');
            $table->dropColumn('link');
            $table->dropColumn('os_version');
            $table->dropColumn('other_link');
            $table->dropColumn('priority');
            $table->dropColumn('reproduse');
        });
    }
};
