-- phpMyAdmin SQL Dump
-- version 5.1.3
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Час створення: Трв 16 2023 р., 00:03
-- Версія сервера: 8.0.29
-- Версія PHP: 7.4.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База даних: `trello2`
--

-- --------------------------------------------------------

--
-- Структура таблиці `bugs`
--

CREATE TABLE `bugs` (
  `id` int UNSIGNED NOT NULL,
  `laboratory_id` int NOT NULL,
  `user_id` bigint UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `date` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `status_id` int DEFAULT NULL,
  `mark_id` int DEFAULT NULL,
  `severity` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `browser` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `os` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `link` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `os_version` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `other_link` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `priority` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reproduse` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `number` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп даних таблиці `bugs`
--

INSERT INTO `bugs` (`id`, `laboratory_id`, `user_id`, `title`, `description`, `date`, `created_at`, `updated_at`, `status_id`, `mark_id`, `severity`, `browser`, `os`, `link`, `os_version`, `other_link`, `priority`, `reproduse`, `number`) VALUES
(1, 2, 1, 'Bug 1', 'test update', '2023-05-13', '2023-05-13 14:44:56', '2023-05-15 15:38:57', 2, 2, 'dwad', '12334', 'dwad', 'dawda', 'dwad', 'dwadwad', 'dawd', 'dwadawdawd', 1),
(2, 2, 1, 'Bug 12', 'Lorem Ipsum', '2023-05-13', '2023-05-13 11:55:26', '2023-05-15 15:39:08', 2, 2, '', '', '', '', '', '', '', '', 2),
(3, 2, 1, 'Bug 123', 'Lorem Ipsum', '2023-05-13', '2023-05-13 18:50:49', '2023-05-14 09:05:02', 2, 1, 'dawda', 'dwadawd', 'dawdad', 'dwadaw', 'dwad', 'dwadawd', 'dwada', 'dwadawd', 3),
(4, 2, 1, 'Bug 114', 'Lorem Ipsum', '2023-05-13', '2023-05-13 18:51:43', '2023-05-14 09:25:19', 2, 1, 'dwadawd', NULL, NULL, NULL, NULL, NULL, NULL, 'Lorem Ipsum', 4),
(5, 2, 1, 'Bug 211', 'Lorem Ipsum', '2023-05-13', '2023-05-13 18:53:01', '2023-05-14 09:02:32', 1, NULL, 'dwad', 'Lorem Ipsum', 'Lorem Ipsum', NULL, NULL, NULL, NULL, 'Lorem Ipsum', 5),
(6, 2, 1, 'Bug 131', 'Lorem Ipsum', '2023-05-13', '2023-05-13 19:03:25', '2023-05-14 09:05:21', NULL, 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6),
(7, 2, 1, 'Bug 121', 'Lorem Ipsum', '2023-05-13', '2023-05-13 19:04:09', '2023-05-13 19:04:09', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 7),
(8, 2, 1, 'Bug 1311', 'Lorem Ipsum', '2023-05-13', '2023-05-13 19:06:37', '2023-05-13 19:06:37', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 8),
(9, 2, 1, 'Bug 1211', 'Lorem Ipsum', '2023-05-13', '2023-05-13 19:06:49', '2023-05-13 19:06:49', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 9),
(10, 2, 1, 'Bug 12131', 'Lorem Ipsum', '2023-05-13', '2023-05-13 19:07:18', '2023-05-13 19:07:18', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 10),
(11, 2, 1, 'Bug 411', 'Lorem Ipsum', '2023-05-13', '2023-05-13 19:08:06', '2023-05-13 19:08:06', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 11),
(12, 2, 1, 'Bug 211', 'Lorem Ipsum', '2023-05-13', '2023-05-13 19:08:47', '2023-05-13 19:08:47', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 12),
(13, 2, 1, 'Bug 112', 'Lorem Ipsum', '2023-05-13', '2023-05-13 19:13:11', '2023-05-13 19:13:11', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 13),
(14, 3, 1, 'Bug 121', 'Lorem Ipsum', '2023-05-14', '2023-05-14 09:20:05', '2023-05-14 09:20:05', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1),
(15, 2, 1, 'Bug 211', 'Lorem Ipsum', '2023-05-14', '2023-05-14 14:36:21', '2023-05-14 14:36:21', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 14),
(16, 2, 12, 'Bug 11', 'Bug 11', '2023-05-15', '2023-05-15 15:27:18', '2023-05-15 17:10:31', 1, 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1),
(17, 2, 12, 'Баг Лого', 'Баг Лого', '2023-05-15', '2023-05-15 17:09:17', '2023-05-15 17:10:44', 2, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2),
(18, 2, 12, 'Баг футер', 'Баг футер', '2023-05-15', '2023-05-15 17:09:30', '2023-05-15 17:09:30', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 3),
(19, 2, 12, 'Баг консоль', 'Баг консоль', '2023-05-15', '2023-05-15 17:09:50', '2023-05-15 17:09:50', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4),
(20, 2, 12, 'Баг кнопки', 'Баг кнопки', '2023-05-15', '2023-05-15 17:10:04', '2023-05-15 17:10:04', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 5);

-- --------------------------------------------------------

--
-- Структура таблиці `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблиці `groups`
--

CREATE TABLE `groups` (
  `id` bigint UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп даних таблиці `groups`
--

INSERT INTO `groups` (`id`, `name`, `created_at`, `updated_at`) VALUES
(2, 'Група 1', '2023-05-13 09:12:18', '2023-05-15 09:56:48'),
(3, 'Група 2', '2023-05-13 09:12:18', '2023-05-15 09:56:56'),
(10, 'Група 3', '2023-05-13 14:39:17', '2023-05-15 09:57:00'),
(11, 'Група 4', '2023-05-13 14:39:41', '2023-05-15 09:57:04');

-- --------------------------------------------------------

--
-- Структура таблиці `laboratory`
--

CREATE TABLE `laboratory` (
  `id` int NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `group_id` int NOT NULL,
  `file` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп даних таблиці `laboratory`
--

INSERT INTO `laboratory` (`id`, `title`, `description`, `created_at`, `updated_at`, `group_id`, `file`) VALUES
(2, 'Лабораторна 1', NULL, NULL, '2023-05-15 11:07:02', 2, '2-1684159622index.txt'),
(3, 'Лабораторна 2', NULL, '2023-05-13 09:34:45', '2023-05-15 11:45:07', 2, '3-1684161907index.txt'),
(6, 'Лабораторна 3', NULL, '2023-05-13 14:20:52', '2023-05-15 11:55:14', 2, '1684162514-222index.txt'),
(7, 'Лабораторна 4', NULL, '2023-05-15 09:56:14', '2023-05-15 16:51:55', 2, '1684180315-_стаття.Бажан.doc');

-- --------------------------------------------------------

--
-- Структура таблиці `marks`
--

CREATE TABLE `marks` (
  `id` bigint UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `color` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп даних таблиці `marks`
--

INSERT INTO `marks` (`id`, `name`, `color`, `created_at`, `updated_at`) VALUES
(1, 'Відмінно (А)', '#FFD600', NULL, NULL),
(2, 'Добре (B)', '#6CBD06', NULL, NULL),
(3, 'Не погано (C)', '#DF2FD8', NULL, NULL),
(4, 'Старайся (Е)', '#F1C4A4', NULL, NULL),
(5, 'Погано (F)', '#F61C1C', NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблиці `migrations`
--

CREATE TABLE `migrations` (
  `id` int UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп даних таблиці `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2016_06_01_000001_create_oauth_auth_codes_table', 1),
(4, '2016_06_01_000002_create_oauth_access_tokens_table', 1),
(5, '2016_06_01_000003_create_oauth_refresh_tokens_table', 1),
(6, '2016_06_01_000004_create_oauth_clients_table', 1),
(7, '2016_06_01_000005_create_oauth_personal_access_clients_table', 1),
(8, '2019_08_19_000000_create_failed_jobs_table', 1),
(9, '2019_12_14_000001_create_personal_access_tokens_table', 1),
(10, '2022_03_21_192026_create_books_table', 1),
(11, '2022_03_21_192049_create_authors_table', 1),
(12, '2022_03_21_193748_create_relation_authors_books_table', 1),
(20, '2022_03_21_192026_create_bugs_table', 2),
(21, '2023_04_27_184725_add_role_to_users_table', 2),
(22, '2023_04_27_185424_add_laboratory_table', 2),
(23, '2023_05_13_114159_create_groups_table', 3),
(24, '2023_05_13_123220_add_laboratory_table', 4),
(25, '2023_05_13_174950_add_laboratory_table', 5),
(26, '2023_05_13_175852_add_group_id_to_user_table', 6),
(27, '2023_05_13_181955_add_columns_to_bugs_table', 7),
(28, '2023_05_13_182649_create_mark_table', 7),
(29, '2023_05_13_182701_create_status_table', 7),
(30, '2023_05_13_185001_add_columns_to_bugs_table', 8),
(31, '2023_05_14_123756_add_columns_to_laboratory_table', 9),
(32, '2023_05_14_152816_create_user_laboratory_mark_table', 10),
(33, '2023_05_14_205957_remove_columns_to_laboratory_table', 11),
(35, '2023_05_15_164850_add_columns_to_users_table', 12);

-- --------------------------------------------------------

--
-- Структура таблиці `oauth_access_tokens`
--

CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint UNSIGNED DEFAULT NULL,
  `client_id` bigint UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп даних таблиці `oauth_access_tokens`
--

INSERT INTO `oauth_access_tokens` (`id`, `user_id`, `client_id`, `name`, `scopes`, `revoked`, `created_at`, `updated_at`, `expires_at`) VALUES
('0106f2fb24cf15b61201d29a3809659dba3ea2ee4b5ac34812543cd0a6eaf61703998a3cf14e3f07', 1, 3, 'Laravel', '[]', 0, '2023-05-11 15:53:12', '2023-05-11 15:53:12', '2024-05-11 18:53:12'),
('062fa6e3c97243db56903335a6083729634e79cc2028a6e2b1caf1b260c8b0f4f42ff79ca33177ce', 1, 3, 'Laravel', '[]', 0, '2023-05-13 16:23:40', '2023-05-13 16:23:40', '2024-05-13 19:23:40'),
('0fb92a8cd879ad86d1a85cc8024e48103434e8035e2829c7fc2fa50ccdfcfe3d3208bdf17feb6449', 1, 3, 'Laravel', '[]', 0, '2023-04-26 07:05:28', '2023-04-26 07:05:28', '2024-04-26 10:05:28'),
('12df1d340e6143f97735d8602f81ab70645a9d30bf9d38eb6cd11c372bb5ced294696c2f28c70a09', 1, 3, 'Laravel', '[]', 0, '2023-04-27 14:44:51', '2023-04-27 14:44:51', '2024-04-27 17:44:51'),
('139482ad37077ddb01f999db1acdecee3462fbff5a4ff37b2205af2351871a8be034a945498cb77d', 12, 3, 'Laravel', '[]', 0, '2023-05-15 16:14:27', '2023-05-15 16:14:27', '2024-05-15 19:14:27'),
('14745522618839338a2e49ba33631d2539b8c0b8e83f2ea71ab339925a5e17d90085307c6743de6a', 12, 3, 'Laravel', '[]', 0, '2023-05-15 17:39:33', '2023-05-15 17:39:33', '2024-05-15 20:39:33'),
('14f9d259f70c64cf14b74ec15443430997b5718a9c27d3b94215aa51f7338ace9dab9abe756ee309', 12, 3, 'Laravel', '[]', 0, '2023-05-15 16:12:26', '2023-05-15 16:12:26', '2024-05-15 19:12:26'),
('179a3a9846efa38242aefce23a37662484d3691d76e740c8a59c337f9020958bc310c9a0c386289e', 1, 3, 'Laravel', '[]', 0, '2023-04-26 07:22:55', '2023-04-26 07:22:55', '2024-04-26 10:22:55'),
('17f3894f4e38cba4d39b769407eda735bf8b3a7d30f2446b6ec32ea488a80c6dbc00dfe8a306c3a3', 2, 3, 'Laravel', '[]', 0, '2023-04-26 13:33:09', '2023-04-26 13:33:09', '2024-04-26 16:33:09'),
('1cac20f502f4a242ae5347363acef002f3ff10adb61fe37c779efb5ef2ada34ed6df08b34707a80f', 12, 3, 'Laravel', '[]', 0, '2023-05-15 16:15:28', '2023-05-15 16:15:28', '2024-05-15 19:15:28'),
('2731f525465f95bfd4dd80a2dea3a4d572873e05f5bae918d9f0103a598a72496a6bf9b530be19cb', 1, 3, 'Laravel', '[]', 0, '2023-04-25 19:04:46', '2023-04-25 19:04:46', '2024-04-25 22:04:46'),
('2c77361165fd0d20dba32abbc27a34c667cb566d2462e0cb4d430c98c6c2122df806b63e61630ff2', 1, 3, 'Laravel', '[]', 0, '2023-05-11 16:23:38', '2023-05-11 16:23:38', '2024-05-11 19:23:38'),
('3015d6591ef69f266d4b579e0645b0a6c4826b17f6e14f3f78d301c095cb31700ab9b778b6ac64c4', 1, 3, 'Laravel', '[]', 0, '2023-05-15 15:32:07', '2023-05-15 15:32:07', '2024-05-15 18:32:07'),
('35010f4bf1f7dcafc45c28718bfd342521ab71a700f408138ae14bded43d6db54bb533410c3ba0ce', 1, 3, 'Laravel', '[]', 0, '2023-05-13 16:16:29', '2023-05-13 16:16:29', '2024-05-13 19:16:29'),
('3dd7193fcf6408745148f27c2dc621f40f4a6ca298568a7625c389afad2829227f2429d6c9c3fa23', 1, 3, 'Laravel', '[]', 0, '2023-05-15 16:12:55', '2023-05-15 16:12:55', '2024-05-15 19:12:55'),
('433b40903bcaa08c1615dbac143a3d8fd2aca3439af731dd58b3fefea5fc45938309596c0150f3a0', 1, 3, 'Laravel', '[]', 0, '2023-05-13 11:32:19', '2023-05-13 11:32:19', '2024-05-13 14:32:19'),
('524837923b39cf143be31db303cdd2a8c1b8aa1ec40dee7fd6a6372cda0bedf1a0cf05e206dcead2', 1, 3, 'Laravel', '[]', 0, '2023-05-15 15:35:28', '2023-05-15 15:35:28', '2024-05-15 18:35:28'),
('535b1a8021ec62e7170a4728915ae683800a9ab13ad8f8eb728220e64a8f6f937648b75142808fb8', 4, 3, 'Laravel', '[]', 0, '2023-04-26 15:02:52', '2023-04-26 15:02:52', '2024-04-26 18:02:52'),
('5685fb1ac6492f6849f915fd1fe932aad981a405f18905f29234cc586a7127451aa0f1dd38762121', 1, 3, 'Laravel', '[]', 0, '2023-04-26 07:04:45', '2023-04-26 07:04:45', '2024-04-26 10:04:45'),
('59135ee1608097765cc8f6653994557622df737b1523ad6e8276acf1fc841aff5e810caa9b7bb79a', 13, 3, 'Laravel', '[]', 0, '2023-05-15 16:45:28', '2023-05-15 16:45:28', '2024-05-15 19:45:28'),
('5939fa2989c774530fd929f952808f304f40edb56534de6df4792667ae2e08cb9be8decd8343e554', 1, 3, 'Laravel', '[]', 0, '2023-05-13 16:24:10', '2023-05-13 16:24:10', '2024-05-13 19:24:10'),
('67a140c699f3a1f9b815135e668c070f9d41846f3bf929587747afa2fabaa3e7472e7b1714e120c9', 3, 3, 'Laravel', '[]', 0, '2023-04-26 13:33:30', '2023-04-26 13:33:30', '2024-04-26 16:33:30'),
('686e5cf9b24742d0317ffc65e551062499fb7d172c2b4ae8b4e19418d2fd4df565b68009effe9de6', 12, 3, 'Laravel', '[]', 0, '2023-05-15 15:59:15', '2023-05-15 15:59:15', '2024-05-15 18:59:15'),
('6960167845ccb7abc8d98aa097dd885c869851dc745c3864de1546831ca1ab39189a8021f34630fa', 1, 3, 'Laravel', '[]', 0, '2023-05-15 17:43:26', '2023-05-15 17:43:26', '2024-05-15 20:43:26'),
('6de42c880f1a7060f9ad5559be651e477d62ddd1bdd492dcf1650d0eaeee06206678b78e94a2cf68', 1, 3, 'Laravel', '[]', 0, '2023-05-15 18:02:43', '2023-05-15 18:02:43', '2024-05-15 21:02:43'),
('73720b12195ea8007874225c7d0c6b8ddaad2399eef358ca83b4caccd35f82e0cea4930e65f5cc65', 1, 3, 'Laravel', '[]', 0, '2023-05-15 15:54:13', '2023-05-15 15:54:13', '2024-05-15 18:54:13'),
('73871437693b29994b5386df5c280d4ef1d23a6289ca0b5bade18b32d8134d81145181de660eb73b', 1, 3, 'Laravel', '[]', 0, '2023-04-27 15:01:05', '2023-04-27 15:01:05', '2024-04-27 18:01:05'),
('73e4b3c388d2b307c27b8fc1a8cfa3c8113f0863b2d3b8d7b1bc7ae1d57efe2b64d3d7cdb7755812', 1, 3, 'Laravel', '[]', 0, '2023-05-13 16:15:08', '2023-05-13 16:15:08', '2024-05-13 19:15:08'),
('76ac9a2e36b74cf979f433fb3be07646da24d5a1c8dc81d1a33d41847e8d4b115934150dab572672', 1, 3, 'Laravel', '[]', 0, '2023-04-26 07:05:42', '2023-04-26 07:05:42', '2024-04-26 10:05:42'),
('7c5ec5330b5754aa1fba617ad1232c297228efd3e12b0f5ad7a36c098f1057bdd82f6b7e05671b09', 1, 3, 'Laravel', '[]', 0, '2023-05-15 15:41:51', '2023-05-15 15:41:51', '2024-05-15 18:41:51'),
('7ca913b9fc141a0166428e9868ed1e0b8c67c21b1f8614a831584776e44164b0a57fb37239f001e8', 1, 3, 'Laravel', '[]', 0, '2023-05-15 13:31:49', '2023-05-15 13:31:49', '2024-05-15 16:31:49'),
('80fa0a17e35ee808b20f6e333e0e870ff424ed07d87072426337e750195c4fd1274038d84e410a44', 1, 3, 'Laravel', '[]', 0, '2023-04-26 07:03:15', '2023-04-26 07:03:15', '2024-04-26 10:03:15'),
('83ec817de627456ec519fdd80a4250f2d3ce7b0d914888a9851ec9dc493a7c62969a359562958787', 1, 3, 'Laravel', '[]', 0, '2023-05-15 16:36:57', '2023-05-15 16:36:57', '2024-05-15 19:36:57'),
('8474e44a7afdf607283e679c26d7ace75a2b3093f94fef03e69cdc80f971f4737a2374188b586ae5', 1, 3, 'Laravel', '[]', 0, '2023-05-13 16:18:40', '2023-05-13 16:18:40', '2024-05-13 19:18:40'),
('8a688d1ebcccf022f958bb70ffe2a9e32a21c0dbf57c31ccb2fa53a54cac8e0c52eee9c95d585298', 1, 3, 'Laravel', '[]', 0, '2023-04-26 13:30:26', '2023-04-26 13:30:26', '2024-04-26 16:30:26'),
('8e303ee9d241fb24977552080e44baa92eea98902b412f9da8d151ecadd8f746d17f83c3176a254f', 12, 3, 'Laravel', '[]', 0, '2023-05-15 15:46:43', '2023-05-15 15:46:43', '2024-05-15 18:46:43'),
('9308660af8f0bf95aab85e858b1f2c64d4ac23f9db3940265f16680750805c9b8cfd8bd322e5263f', 1, 3, 'Laravel', '[]', 0, '2023-04-25 18:56:37', '2023-04-25 18:56:37', '2024-04-25 21:56:37'),
('9a69c9dc5c8aeab4abae83a568a9ad57ceddfa0331f06171abb2b7d97f3e8126335c11c19718f1c4', 12, 3, 'Laravel', '[]', 0, '2023-05-15 17:07:26', '2023-05-15 17:07:26', '2024-05-15 20:07:26'),
('9c3dd2385010166f05ec847a18484aca922f948058540e20a136b4f75caae454da18beaecc093141', 1, 3, 'Laravel', '[]', 0, '2023-05-15 16:51:06', '2023-05-15 16:51:06', '2024-05-15 19:51:06'),
('a0416870c70ab2426f2bfab9cec368c40f2cc44214c59f1bbb3dea289c8fab47d8e5d7d63a36336f', 1, 3, 'Laravel', '[]', 0, '2023-05-11 16:27:23', '2023-05-11 16:27:23', '2024-05-11 19:27:23'),
('b06d629a45f6cd09ef352fc3ce388af296cc0992812c25420503d1641ec4ba96055a3478677107e8', 12, 3, 'Laravel', '[]', 0, '2023-05-15 15:05:55', '2023-05-15 15:05:55', '2024-05-15 18:05:55'),
('b19f3b9cee483ebfedc9167ad1a58796cde69a515315ff1125bc42d8f9323f23e9fef9d6c2367124', 1, 3, 'Laravel', '[]', 0, '2023-05-15 16:11:28', '2023-05-15 16:11:28', '2024-05-15 19:11:28'),
('b1c4e1ccd6ae0548f97e560f2368eb7f459314c42512679ee76368349724e6b5dcb8cdfd9a0b3fd7', 1, 3, 'Laravel', '[]', 0, '2023-05-15 16:22:42', '2023-05-15 16:22:42', '2024-05-15 19:22:42'),
('b5e162d499a518378ac55db8470ccd2534774a104c853a3801abdbb072e9ed0dc66557f281727ed7', 1, 3, 'Laravel', '[]', 0, '2023-04-25 17:13:14', '2023-04-25 17:13:14', '2024-04-25 20:13:14'),
('bee252e0056483c47855add74ee4c4e8dc22287a869bffe4cc74e9edebaa6e2f76f0b4058cba66a5', 1, 3, 'Laravel', '[]', 0, '2023-05-15 18:03:16', '2023-05-15 18:03:16', '2024-05-15 21:03:16'),
('bf591761bc6df5d5b12376807d908301eeee4931ea5b7753d283771f244812822f688cde6e251324', 1, 3, 'Laravel', '[]', 0, '2023-04-26 07:22:19', '2023-04-26 07:22:19', '2024-04-26 10:22:19'),
('cd165666bf1b0f2c2f5812bc63a8df47c9f628843ae4ac1fe0c8e1db50ad1d5b458fdc13cc4f2c31', 1, 3, 'Laravel', '[]', 0, '2023-05-13 16:18:48', '2023-05-13 16:18:48', '2024-05-13 19:18:48'),
('d18b4284cd296a8505786d0872ba3c34fe31cc9c90ebeee58a15497cb9018c8378933443ac2d0675', 1, 3, 'Laravel', '[]', 0, '2023-05-15 15:35:16', '2023-05-15 15:35:16', '2024-05-15 18:35:16'),
('d3af8d5856c1c433f2a0c515c88a704b0480b70888d6d3e7219fb9ee8b551f3a75e851953eae9d31', 1, 3, 'Laravel', '[]', 0, '2023-05-15 15:32:33', '2023-05-15 15:32:33', '2024-05-15 18:32:33'),
('d9933821b6073d60d2fa309254cba1e5ab46e3de47453bfb0951e91332dc8111909df98222fbc27f', 1, 3, 'Laravel', '[]', 0, '2023-05-15 16:15:39', '2023-05-15 16:15:39', '2024-05-15 19:15:39'),
('e4960b15746ee98dfb0e51215c239a1ced1c127ba690ad76152036e2bdfa9a98415b108849fc9d47', 1, 3, 'Laravel', '[]', 0, '2023-05-13 16:17:17', '2023-05-13 16:17:17', '2024-05-13 19:17:17'),
('e6ada929edeb34276c165818c91219c5cda46c8ab5c3eb4bfdc576640f55ba19bb26132b97b47172', 1, 3, 'Laravel', '[]', 0, '2023-04-26 07:04:30', '2023-04-26 07:04:30', '2024-04-26 10:04:30'),
('f1633db006e83635593735a852829753c5d079a06f6cb1b24f8dcd2c4e9874d47e5c65839705c540', 12, 3, 'Laravel', '[]', 0, '2023-05-15 15:55:08', '2023-05-15 15:55:08', '2024-05-15 18:55:08'),
('f1b60c89c9b1c33050404c90a266a7ce04c1769cf2257170dc4fc18d8808809e60c8b59057cf3c41', 1, 3, 'Laravel', '[]', 0, '2023-05-15 16:14:47', '2023-05-15 16:14:47', '2024-05-15 19:14:47'),
('f4014d9c3b6d9f881451803330c6de691dde0b3119be73b88f9a8889ccab9e44cfe93cf4b0eb2851', 1, 3, 'Laravel', '[]', 0, '2023-05-15 17:10:16', '2023-05-15 17:10:16', '2024-05-15 20:10:16'),
('f8a6032d85c0b7f0f748f3c6c2efbf0e974b38e250d9b0e029ea71690e42880dddc1e33e896f73e1', 1, 3, 'Laravel', '[]', 0, '2023-05-15 15:58:29', '2023-05-15 15:58:29', '2024-05-15 18:58:29'),
('fc6612430b97d715ff6ad9e3c30aa006781de5027a88660fae6beb98afdbff47c30d521bab0201b9', 1, 3, 'Laravel', '[]', 0, '2023-05-15 12:24:41', '2023-05-15 12:24:41', '2024-05-15 15:24:41'),
('fc9b52e2c51a6b4223da38b0e0bd8f93c1b353c26ba0cc1f3aca576c6cf2ce783068922b7c48eee2', 1, 3, 'Laravel', '[]', 0, '2023-04-25 17:09:16', '2023-04-25 17:09:16', '2024-04-25 20:09:16');

-- --------------------------------------------------------

--
-- Структура таблиці `oauth_auth_codes`
--

CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint UNSIGNED NOT NULL,
  `client_id` bigint UNSIGNED NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблиці `oauth_clients`
--

CREATE TABLE `oauth_clients` (
  `id` bigint UNSIGNED NOT NULL,
  `user_id` bigint UNSIGNED DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provider` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп даних таблиці `oauth_clients`
--

INSERT INTO `oauth_clients` (`id`, `user_id`, `name`, `secret`, `provider`, `redirect`, `personal_access_client`, `password_client`, `revoked`, `created_at`, `updated_at`) VALUES
(1, NULL, 'Laravel Personal Access Client', 'F1djvQvvklEMyaG2aS4baTKERjkz4jRiCS8nADEz', NULL, 'http://localhost', 1, 0, 0, '2023-04-25 17:08:36', '2023-04-25 17:08:36'),
(2, NULL, 'Laravel Password Grant Client', 'fBNinkfr2iJyMTwxRha4bj1MAlNo1EmduKHp2egq', 'users', 'http://localhost', 0, 1, 0, '2023-04-25 17:08:36', '2023-04-25 17:08:36'),
(3, NULL, 'Admin', 'g4LjYYsz97MgDkWQdxdnseYkeE6pa8DQnrj0y2Vu', NULL, 'http://localhost', 1, 0, 0, '2023-04-25 17:09:11', '2023-04-25 17:09:11');

-- --------------------------------------------------------

--
-- Структура таблиці `oauth_personal_access_clients`
--

CREATE TABLE `oauth_personal_access_clients` (
  `id` bigint UNSIGNED NOT NULL,
  `client_id` bigint UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп даних таблиці `oauth_personal_access_clients`
--

INSERT INTO `oauth_personal_access_clients` (`id`, `client_id`, `created_at`, `updated_at`) VALUES
(1, 1, '2023-04-25 17:08:36', '2023-04-25 17:08:36'),
(2, 3, '2023-04-25 17:09:11', '2023-04-25 17:09:11');

-- --------------------------------------------------------

--
-- Структура таблиці `oauth_refresh_tokens`
--

CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблиці `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблиці `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint UNSIGNED NOT NULL,
  `tokenable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблиці `status`
--

CREATE TABLE `status` (
  `id` bigint UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп даних таблиці `status`
--

INSERT INTO `status` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Повернено', NULL, NULL),
(2, 'Виконано', NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблиці `users`
--

CREATE TABLE `users` (
  `id` bigint UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `role` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'student',
  `group_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '0',
  `activation_token` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп даних таблиці `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`, `role`, `group_id`, `active`, `activation_token`) VALUES
(1, 'Admin', 'dgemm@ex.ua', NULL, '$2y$10$R/Tk1iCySYYEu3yohXp5X.KkG2PKkTBh9eIligDyK8IGiLAiZ77WC', NULL, '2023-04-25 17:06:24', '2023-04-25 17:06:24', 'teacher', '2', 1, NULL),
(2, 'Федечко Божко', 'dgemm2@ex.ua', NULL, '$2y$10$Ni8Njv7gaGpCfJoRz9s2q.odG0xvXW1jmiBosMk/KEO9snwk4To9q', NULL, '2023-04-26 13:33:05', '2023-04-26 13:33:05', 'student', '2', 1, NULL),
(3, 'Шуляк Владислав', 'dgemm3@ex.ua', NULL, '$2y$10$gAoXzba3wl6AEnTrJqF7rOMJTFaAw3m5Nv.M7Il6NCJ8UKz7Ukwia', NULL, '2023-04-26 13:33:26', '2023-04-26 13:33:26', 'student', '2', 1, NULL),
(4, 'Баліцкий Юрій Васильович', 'urijbalickij185@gmail.com', NULL, '$2y$10$PsUoAwd.W4nsYxlSrMVMYuHDgFc3WzXsxvdqvH5UBhNS0b4q4Vece', NULL, '2023-04-26 15:02:04', '2023-04-26 15:02:04', 'student', '2', 1, NULL),
(5, 'Горак Гаїна', 'dgemm213123@ex.ua', NULL, '$2y$10$G8OE.re19P2wNOugX4i08uI.OqlC2NekAhc.eoetsF9i52VeqasiK', NULL, '2023-05-15 12:29:48', '2023-05-15 12:29:48', 'student', '2', 1, NULL),
(6, 'Борисенко Ничипір', 'dgemm2131233@ex.ua', NULL, '$2y$10$U1qa8AO2UkGS8416.9g.lOP0sIbn6utM4FfrsHc5f9RTa8I7Np9Ie', NULL, '2023-05-15 12:29:59', '2023-05-15 12:29:59', 'student', '2', 1, NULL),
(7, 'Козяр Жозефіна', '12312dgemm@ex.ua', NULL, '$2y$10$kL5Bl2r/SBNjqD6dsLnMv.U4ImnsA7J0/W8wpzqvHVBQGyxiYUzxa', NULL, '2023-05-15 12:30:24', '2023-05-15 12:30:24', 'student', '2', 1, NULL),
(8, 'Шутко Христофор', '123123dgemm@ex.ua', NULL, '$2y$10$dB0Bp0cqNmDvwlLG4A7KaeIdDvYZgLIrAWvpplCb8xtUG2aaKPZlO', NULL, '2023-05-15 12:31:12', '2023-05-15 12:31:12', 'student', '2', 1, NULL),
(9, 'Тимочко Еммануїл', 'dgemm1212312@ex.ua', NULL, '$2y$10$rjLyuISJvEd8PxBNusCBtuvm6VPgo6/PyYSfBELp6bFURAbXpOXY.', NULL, '2023-05-15 14:08:23', '2023-05-15 14:08:23', 'student', '2', 0, NULL),
(10, 'Карплюк Юліан', 'dgemsfgsm@ex.ua', NULL, '$2y$10$2bujOxNkr566Hv/TybaXwe5nfyuV2axF5nOUUkpRZJps6IW717Hk.', NULL, '2023-05-15 14:09:22', '2023-05-15 14:09:22', 'student', '2', 0, '$2y$10$Z2cy0KUXmzg5ajQrnaH6Ru5IS8SqncN8dkGKYF0c572gILeUESfcm'),
(11, 'Онопенко Цвітана', 'dgdsadsaemm@ex.ua', NULL, '$2y$10$DBWFs16dAPJD0xSW38oq.esCe0AoTx881Iahte5nbKz7O7t3yBEdS', NULL, '2023-05-15 14:49:43', '2023-05-15 14:49:43', 'student', '2', 0, '$2y$10$epZn.Z.2daAq2Oug6TpNi.Xb.svW7wuCSr1chaeR.ua454FklKUpu'),
(12, 'Юрій М.', 'uurrian@gmail.com', NULL, '$2y$10$Lsx5tO1s8wvddRN3wmJXmuVT6lH8EEwwy.1y6Mt7QgJUYQe9IIL/2', NULL, '2023-05-15 15:05:01', '2023-05-15 15:08:37', 'student', '2', 1, '$2y$10$.A05sSz6Ezpw86N3pQd9fO5Yc4pCs1x9mdPx1Y3S8hQkU8Ob3Wv7C'),
(13, 'Бажан Вікторія Михайлівна', 'vb@winstars.tech', NULL, '$2y$10$MCJnGjR19JaIgRTFxKeln.CdlvZnkkvCH/5MUasbrM4AUWZcDjwT.', NULL, '2023-05-15 16:40:54', '2023-05-15 16:42:51', 'student', '2', 1, '$2y$10$ZeIhdnEa2Z9gXCfr2I9B..9kUkUb0ygek7LYxoztvE8FcUfGpsidS');

-- --------------------------------------------------------

--
-- Структура таблиці `user_laboratory_mark`
--

CREATE TABLE `user_laboratory_mark` (
  `id` bigint UNSIGNED NOT NULL,
  `user_id` int NOT NULL,
  `laboratory_id` int NOT NULL,
  `mark_id` int NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп даних таблиці `user_laboratory_mark`
--

INSERT INTO `user_laboratory_mark` (`id`, `user_id`, `laboratory_id`, `mark_id`, `created_at`, `updated_at`) VALUES
(1, 1, 2, 2, '2023-05-14 18:46:47', '2023-05-15 17:31:05'),
(2, 1, 3, 5, '2023-05-15 08:23:00', '2023-05-15 09:34:30'),
(3, 12, 2, 1, '2023-05-15 17:37:27', '2023-05-15 17:37:27');

--
-- Індекси збережених таблиць
--

--
-- Індекси таблиці `bugs`
--
ALTER TABLE `bugs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `laboratory_id` (`laboratory_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Індекси таблиці `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Індекси таблиці `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`);

--
-- Індекси таблиці `laboratory`
--
ALTER TABLE `laboratory`
  ADD PRIMARY KEY (`id`);

--
-- Індекси таблиці `marks`
--
ALTER TABLE `marks`
  ADD PRIMARY KEY (`id`);

--
-- Індекси таблиці `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Індекси таблиці `oauth_access_tokens`
--
ALTER TABLE `oauth_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_access_tokens_user_id_index` (`user_id`);

--
-- Індекси таблиці `oauth_auth_codes`
--
ALTER TABLE `oauth_auth_codes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_auth_codes_user_id_index` (`user_id`);

--
-- Індекси таблиці `oauth_clients`
--
ALTER TABLE `oauth_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_clients_user_id_index` (`user_id`);

--
-- Індекси таблиці `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  ADD PRIMARY KEY (`id`);

--
-- Індекси таблиці `oauth_refresh_tokens`
--
ALTER TABLE `oauth_refresh_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`);

--
-- Індекси таблиці `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Індекси таблиці `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- Індекси таблиці `status`
--
ALTER TABLE `status`
  ADD PRIMARY KEY (`id`);

--
-- Індекси таблиці `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Індекси таблиці `user_laboratory_mark`
--
ALTER TABLE `user_laboratory_mark`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для збережених таблиць
--

--
-- AUTO_INCREMENT для таблиці `bugs`
--
ALTER TABLE `bugs`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT для таблиці `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблиці `groups`
--
ALTER TABLE `groups`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT для таблиці `laboratory`
--
ALTER TABLE `laboratory`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT для таблиці `marks`
--
ALTER TABLE `marks`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT для таблиці `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT для таблиці `oauth_clients`
--
ALTER TABLE `oauth_clients`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT для таблиці `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT для таблиці `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблиці `status`
--
ALTER TABLE `status`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT для таблиці `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT для таблиці `user_laboratory_mark`
--
ALTER TABLE `user_laboratory_mark`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Обмеження зовнішнього ключа збережених таблиць
--

--
-- Обмеження зовнішнього ключа таблиці `bugs`
--
ALTER TABLE `bugs`
  ADD CONSTRAINT `bugs_ibfk_1` FOREIGN KEY (`laboratory_id`) REFERENCES `laboratory` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT `bugs_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
