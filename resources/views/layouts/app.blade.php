<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" value="{{ csrf_token() }}"/>
    <title>Розробка програмного застосунку для документування та відслідковування стану виправлення дефектів програмного забезпечення</title>
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="/css/style.css">
</head>
<body>
<div id="app">
    <App></App>
</div>
<script src="/js/app.js" type="text/javascript"></script>
</body>
</html>
