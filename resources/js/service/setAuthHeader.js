import Axios from "axios";

const axios = Axios.create({
baseURL: '',
  headers: {
    Accept: "application/json",
    "Content-Type": "application/json",
    responseType: "json",
  },
});

axios.interceptors.request.use((config) => {
  const token = localStorage.getItem('token');
  if (!token) {
    return config;
  }

  config.headers.Authorization = `Bearer ${token}`;

  return config;
});
export default axios
