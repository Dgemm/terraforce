import { createApp } from "vue";
import router from "./router";
import App from './App.vue';


// Import Bootstrap an BootstrapVue CSS files (order is important)
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'


router.beforeEach(async (to, from) => {
  if (
    // make sure the user is authenticated
    !localStorage.getItem('token') &&
    // ❗️ Avoid an infinite redirect
    ( to.name !== 'login'
        && to.name !== 'register'
        && to.name !== 'email'
        && to.name !== 'home'
    )
  ) {
    // redirect the user to the login page
    return { name: 'home' }
  }
})


createApp({
    components: {
        App,
    },
}).use(router)
    .mount("#app");


