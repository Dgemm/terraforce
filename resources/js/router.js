import { createRouter, createWebHistory } from "vue-router";
import Register from './pages/Register'
import Login from './pages/Login'
import Logout from './components/Logout'

import Home from "./pages/Home";
import EmailNote from "./pages/EmailNote";
import CabinetStudent from './pages/CabinetStudent'
import CabinetTeacherLabs from './pages/CabinetTeacherLabs'
import CreateLab from "./pages/CreateLab";
import BugForm from "./pages/BugForm";
import CabinetTeacher from "./pages/CabinetTeacher";
import Results from "./pages/Results";

const routes = [
    {
        path: "/",
        name: "home",
        component: Home,
    },
    {
        path: "/login",
        name: "login",
        component: Login,
    },
    {
        path: "/logout",
        name: "logout",
        component: Logout,
    },
    {
        path: "/register",
        name: "register",
        component: Register,
    },
    {
        path: "/email",
        name: "email",
        component: EmailNote,
    },
    {
        path: "/cabinet",
        name: "cabinet",
        component: CabinetStudent,
    },
    {
        path: "/cabinet-teacher",
        name: "cabinet-teachers",
        component: CabinetTeacher,
    },
    {
        path: "/cabinet-labs",
        name: "cabinet-labs",
        component: CabinetTeacherLabs,
    },
    {
        path: "/result",
        name: "result",
        component: Results,
    },
    {
        path: "/create-lab",
        name: "create-lab",
        component: CreateLab,
    },
    {
        path: "/bugs",
        name: "bugs",
        component: CabinetStudent,
    },
    {
        path: '/bug/:id/:laboratoryId',
        name: 'BugForm2',
        component: BugForm,
    },
     {
         path: '/bugs/form/:laboratoryId',
         name: 'BugForm',
         component: BugForm,
     },
];



export default createRouter({
    history: createWebHistory(),
    routes,
});

