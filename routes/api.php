<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\Auth\RegisterController;
use App\Http\Controllers\Api\Auth\LoginController;
use App\Http\Controllers\Api\GroupController;
use App\Http\Controllers\Api\LaboratoryController;
use App\Http\Controllers\Api\BugsController;
use App\Http\Controllers\Api\MarkController;
use App\Http\Controllers\Api\StatusController;
use App\Http\Controllers\Api\UserController;
use App\Http\Controllers\Api\Auth\ActivatorController;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('activate/{activaToken}', [ActivatorController::class, 'activate'])->name('activate');

Route::post('/register', RegisterController::class);
Route::post('/login', [ 'as' => 'login', 'uses' => LoginController::class]);

Route::resource('groups', GroupController::class)->only([
    'index', 'store', 'update', 'destroy'
]);


Route::middleware('auth:api')->group(function () {
    Route::post('logout', '\App\Http\Controllers\Api\Auth\LogoutController');

    Route::resource('laboratory', LaboratoryController::class)->only([
        'index', 'show', 'store', 'update', 'destroy'
    ]);
    Route::patch('laboratory-mark', [LaboratoryController::class, 'updateMark'])->name('laboratory.updateMark');
    Route::post('laboratory-upload/{laboratory}', [LaboratoryController::class, 'uloadFile'])->name('laboratory.updateMark');
    Route::get('laboratory-download/{laboratory}', [LaboratoryController::class, 'downloadFile'])->name('laboratory.down');

    Route::resource('bugs', BugsController::class)->only([
        'index', 'show', 'store', 'update', 'destroy'
    ]);
    Route::put('bugs/{bug}/mark', [BugsController::class, 'updateMark'])->name('bugs.updateMark');
    Route::put('bugs/{bug}/status', [BugsController::class, 'updateStatus'])->name('bugs.updateStatus');

    Route::get('/marks', [MarkController::class, 'index']);
    Route::get('/status', [StatusController::class, 'index']);

    Route::get('/users', [UserController::class, 'index']);
});
URL::forceScheme('https');


