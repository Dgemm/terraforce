<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Laboratory extends Model
{
    use HasFactory;

    protected $table = 'laboratory';
    protected $guarded = [];
    protected $appends = ['fileDown'];
    protected $hidden = ['file'];

    public function getFileDownAttribute()
    {
        if($this->file)
            return route('laboratory.down', ['laboratory' => $this->id]);
        else
            return null;
    }
}
