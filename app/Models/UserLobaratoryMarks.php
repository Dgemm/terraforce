<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserLobaratoryMarks extends Model
{
    use HasFactory;
    protected $table  = 'user_laboratory_mark';

    protected $guarded = [];
}
