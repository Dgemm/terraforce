<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Bugs extends Model
{
    use HasFactory;

    protected $guarded = ['id', 'created_at'];


    public function users(){
        return $this->belongsToMany('App\Models\users');
    }
}
