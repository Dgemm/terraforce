<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Bugs;
use Illuminate\Support\Facades\Auth;

class BugsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $bugs = Bugs::query();

        if($request->labId)
            $bugs->where('laboratory_id', $request->labId);

        if(Auth::user()->role == 'student'){
            $bugs->where('user_id', Auth::id());
        } else {
            if($request->user_id)
                $bugs->where('user_id', $request->user_id);
        }

        return $bugs->get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $userId = Auth::id();
        $laboratory_id = $request->laboratory_id;
        $count = Bugs::where([
            'user_id' => $userId,
            'laboratory_id' => $laboratory_id,
        ])->count();
        $bug = Bugs::create(
            array_merge(
                $request->only([
                    'title',
                    'description',
                    'date',
                    'browser',
                    'link',
                    'link',
                    'os',
                    'os_version',
                    'other_link',
                    'priority',
                    'reproduse',
                    'severity',
                    'laboratory_id']),
                [
                    'user_id' => Auth::id(),
                    'date' => now(),
                    'number' => $count+1
                ]
            )
        );
        return $bug;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Bugs::findOrFail($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Bugs  $Bugs
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Bugs $Bug)
    {
        $Bug->update($request->only([
            'title',
            'description',
            'date',
            'browser',
            'link',
            'link',
            'os',
            'os_version',
            'other_link',
            'priority',
            'reproduse',
            'severity',
        ]));
        return $Bug;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Bugs  $Bugs
     * @return \Illuminate\Http\Response
     */
    public function updateMark(Request $request, Bugs $Bug)
    {
        $Bug->update($request->only([
            'mark_id',
        ]));
        return $Bug;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Bugs  $Bugs
     * @return \Illuminate\Http\Response
     */
    public function updateStatus(Request $request, Bugs $Bug)
    {
        $Bug->update($request->only([
            'status_id',
        ]));
        return $Bug;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Bugs $Bugs)
    {
        $Bugs->delete();
        return $this->index();
    }
}
