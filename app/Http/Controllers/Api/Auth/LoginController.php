<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use App\Models\User;

class LoginController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $credentials = $request->only('email', 'password');

        $user = User::where('email', $request->email)->first();

        if(!$user->active)
            return response()->json([
                'message' => 'Your account has not been activated yet',
                'errors' => 'Unauthorised'
            ]);

        if (!Auth::attempt($credentials)) {
            return response()->json([
                'message' => 'You cannot sign with those credentials',
                'errors' => 'Unauthorised'
            ]);
        }

        $token = Auth::user()->createToken(config('app.name'));

        $token->token->save();

        return response()->json([
            'token_type' => 'Bearer',
            'name' => Auth::user()->name,
            'role' => Auth::user()->role,
            'groupId' => Auth::user()->group_id,
            'token' => $token->accessToken,
            'expires_at' => Carbon::parse($token->token->expires_at)->toDateTimeString(),
        ], 200);
    }
}
