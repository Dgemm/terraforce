<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Api\Auth\ActivatorController;

class RegisterController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255',
            'email' => 'required|unique:users,email',
            'password' => 'required',
            'group_id' => 'required',
        ]);

        if ($validator->fails()) {
            return ['errors' => $validator->errors()];
        }


        $user = User::create(array_merge(
            $request->only('name', 'email', 'group_id'),
            [
                'password' => bcrypt($request->password),
                'activation_token' => bcrypt($request->email)
            ],
        ));

        (new ActivatorController())->sendEmail($user);


        return response()->json([
            'message' => 'You were successfully registered. Use your email and password to sign in.'
        ], 200);
    }
}
