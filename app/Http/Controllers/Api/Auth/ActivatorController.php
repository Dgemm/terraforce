<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class ActivatorController extends Controller
{
    public function sendEmail(User $user)
    {
        $link = route('activate', ['activaToken' => $user->activation_token]);
        $text = "Новий користувач $user->name ($user->email) зареєструвався на сайті, для активації перейдіть по посиланню ". $link;


        Mail::raw($text, function ($message) {
            $message->to(env('MAIL_USERNAME'))
                ->subject('Активація нового користувача');
        });
    }

    public function activate(Request $request)
    {
        $user = User::where('activation_token', $request->activaToken)->firstOrFail();
        $user->update([
            'active' => true
        ]);

        $text = "Ваш аккаунт успішно активований на " . url('/');

        Mail::raw($text, function ($message) use ($user) {
            $message->to($user->email)
                ->subject('Активація аккаунта');
        });

        return redirect('/');

    }
}
