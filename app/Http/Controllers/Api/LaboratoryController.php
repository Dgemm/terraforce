<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Laboratory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Models\UserLobaratoryMarks;

class LaboratoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $laboratory = Laboratory::query();
        if(Auth::user()->role == 'student'){
            $laboratory->where('group_id', Auth::user()->group_id);
            $userID = Auth::id();
            $laboratory->leftJoin('user_laboratory_mark', function($join) use ($userID)
            {
                $join->on('user_laboratory_mark.laboratory_id', '=', 'laboratory.id');
                $join->on('user_laboratory_mark.user_id', '=', DB::raw($userID));

            });

            $laboratory->select('laboratory.*', 'user_laboratory_mark.mark_id');

        } else {

            if($request->groupId)
                $laboratory->where('group_id', $request->groupId);

            if($request->user_id){
                $userID = $request->user_id;
                $laboratory->leftJoin('user_laboratory_mark', function($join) use ($userID)
                {
                    $join->on('user_laboratory_mark.laboratory_id', '=', 'laboratory.id');
                    $join->on('user_laboratory_mark.user_id', '=', DB::raw($userID));

                });

                $laboratory->select('laboratory.*', 'user_laboratory_mark.mark_id');
            }
        }


        return $laboratory->get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $laboratory = new Laboratory();
        $laboratory->title = $request->title;
        $laboratory->description = $request->description;
        $laboratory->group_id = $request->group_id;
        $laboratory->save();
        return $laboratory;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Laboratory  $Laboratory
     * @return \Illuminate\Http\Response
     */
    public function show(Laboratory $Laboratory)
    {
        return $Laboratory;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Laboratory  $Laboratory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Laboratory $Laboratory)
    {
        $Laboratory->title = $request->title;
        $Laboratory->description = $request->description;
        $Laboratory->save();
        return $Laboratory;
    }



    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Bugs  $Bugs
     * @return \Illuminate\Http\Response
     */
    public function updateMark(Request $request)
    {
        $result = UserLobaratoryMarks::updateOrCreate([
            'user_id'   => $request->user_id,
            'laboratory_id' => $request->labId
        ],[
            'mark_id' => $request->mark_id
        ]);

        if(!$result)
            return [];

        $userID = $request->user_id;

        $lab = Laboratory::query()
            ->leftJoin('user_laboratory_mark', function($join) use ($userID)
            {
                $join->on('user_laboratory_mark.laboratory_id', '=', 'laboratory.id');
                $join->on('user_laboratory_mark.user_id', '=', DB::raw($userID));

            })
            ->select('laboratory.*', 'user_laboratory_mark.mark_id')
            ->where('laboratory.id', $request->labId)
            ->get();

        return $lab;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Laboratory  $Laboratory
     * @return \Illuminate\Http\Response
     */
    public function uloadFile(Request $request,  Laboratory $Laboratory)
    {
        if (!$request->hasFile('file'))
            return response()->json(['error'=>'No file uploaded.']);

        $file = $request->file('file');

        $fileName =  time() . '-' . $file->getClientOriginalName();

        $request->file('file')->storeAs('public/uploads', $fileName);

        $Laboratory->file = $fileName;
        $Laboratory->save();

        return response()->json(['status'=>'File uploaded successfully.']);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Laboratory  $Laboratory
     * @return \Illuminate\Http\Response
     */
    public function downloadFile(Laboratory $Laboratory)
    {
        if(!$Laboratory)
            return;

        $filePath = public_path('storage/uploads/'.$Laboratory->file, $Laboratory->file);

        if (file_exists($filePath))
            return response()->download($filePath);
        else
            echo 'error';exit;

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Laboratory  $Laboratory
     * @return \Illuminate\Http\Response
     */
    public function destroy(Laboratory $Laboratory)
    {
        $Laboratory->delete();
        return $this->index();
    }
}
