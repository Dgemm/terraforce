<?php

namespace App\Http\Requests\Api\Auth;

use Illuminate\Foundation\Http\FormRequest;

class RegisterFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255',
            'password' => 'required|string|min:6|confirmed'
        ];
    }
    public function messages()
    {
        return [
            'name.required' => 'A title is required',
            'email.required' => 'A message is required',
            'password.required' => 'A message is required',
            'confirmed.required' => 'A message is required',
        ];
    }
}
